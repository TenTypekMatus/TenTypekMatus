- Country of origin: 🇸🇰 
- Launguages: C++, C, Rust, Shell script (Bash), Ansible

| Language | Proficiency                                             |
| ------   | ------                                                  |
| C++      | Advanced                                                |
| C        | Advanced                                                |
| Rust     | Advanced to native (my preferred programming language)  |
|Bash      | Intermediate                                            |
|Ansible   | Advanced                                                |
## Social links:
- <a rel="me" href="https://fosstodon.org/@ThatGuyMatus">Mastodon</a>
- [GitHub](https://github.com/DasMatus)
- [My blog in Slovak](https://tentypekmatus.blogspot.com)
## Notable projects that I've done:
- [PM](https://gitlab.com/MatuushOS/pm)
- [MtBuild](https://gitlab.com/matuushos/mtbuild)
## Contributed to:
- [cdupage](https://gitlab.com/ivanhrabcak/cdupage)
    - Mostly porting some stuff from the Python side